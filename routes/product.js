var express = require('express');
var router = express.Router();
const bcrypt=require('bcrypt');

const checkAuth = require('../middleware/check-auth');
const roleCheck = require('../middleware/role-check');
const {addProduct,get_all_products,deleteProduct,updateProduct} = require('../controller/product');

router.get('/', get_all_products);
router.post('/', addProduct);
router.delete('/', addProduct);
router.patch('/', updateProduct);





module.exports = router;