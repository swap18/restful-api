const jwt = require('jsonwebtoken');
const { token } = require('morgan');
const User = require('../model/user');

module.exports = async (req, res, next) => {
    try {
        const token = await req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, "secret");
        req.userData = decoded;
        next();
    }
    catch (error) {
        res.status(401).json({
            message: 'Auth Failed'
        });

    }




}

