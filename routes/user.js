var express = require('express');
var router = express.Router();
const bcrypt=require('bcrypt');
const {userSignup,get_all_users,deleteUser,user_Login,updateUser} = require('../controller/user');
const checkAuth = require('../middleware/check-auth');
const roleCheck = require('../middleware/role-check');


router.post('/signup', userSignup)
router.get('/', get_all_users)
router.delete('/',checkAuth,roleCheck, deleteUser)
router.post('/login',user_Login) 
router.patch('/:id',checkAuth,roleCheck,updateUser) 



module.exports = router;