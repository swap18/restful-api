const mongoose=require('mongoose');

const productSchema=new mongoose.Schema({
    title:{
        type:String,
        required:true,
        
    },
    description:{
        type:String,
        required:true,
       
    },
    categories:{type:mongoose.Schema.Types.ObjectId,required:true},
    size:{type:String},
    color:{type:String},
    price:{type:Number,required:true},

   
},{timestamps:true})
module.exports=mongoose.model('Product',productSchema)