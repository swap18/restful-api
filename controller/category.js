const Category = require('../model/category');


exports.getCategory = async (req, res, next) => {
    let category = await Category.find()
    return res.status(200).json({ message: 'get success', data: [{ category }] })
}
exports.addCategory = async (req, res, next) => {
    console.log(req.file); 
    const { name } = req.body
    let category = await Category.create({ name })
    return res.status(200).json({ message: 'add success', data: [{ category }] })
}

exports.updateCategory = async (req, res, next) => {
    
    const { name } = req.body
    const {id}= req.query
    let category = await Category.findByIdAndUpdate(id, {name:name })
    return res.status(200).json({ message: 'Update success', data: [{ category }] })
}
exports.deleteCategory = async (req, res, next) => {
    
    const { name } = req.body
    const {id}= req.query
    let category = await Category.findByIdAndRemove(id)
    return res.status(200).json({ message: 'Delete success', data: [{ category }] })
}