var express = require('express');
var router = express.Router();

const category=require('./category')
const user=require('./user')
const product=require('./product')

router.use('/category',category);
router.use('/user',user);
router.use('/product',product);


module.exports = router;
