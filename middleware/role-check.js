const User=require('../model/user');

module.exports= async(req,res,next)=>{
    try{
        const adminData= await User.findById(req.userData.id);

        if(adminData.isAdmin){
            next();
           
        }
        else{
            return res.status(500).json({message:' You need to be admin to do this'})
        }
        
    }
    catch(err){
        return res.status(500).json({message:'failed to authorize'})
    }



}