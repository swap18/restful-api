const Product = require('../model/Product');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')



exports.addProduct = async (req, res, next) => {
    console.log(req.file); 
    const{title,description,categories,size,color,price}=req.body
    let product = await Product.create({title,description,categories,size,color,price})
    return res.status(200).json({ message: 'Product Added', data: [{ product }] })
}

exports.get_all_products = async (req, res, next) => {
    let product = await Product.find()
    return res.status(200).json({ message: 'All Products fetched', data: [{ product }] })
}



exports.deleteProduct = async (req, res, next) => {


    const { id } = req.query
    let product = await Product.findByIdAndRemove(id)
    return res.status(200).json({ message: 'Product Deleted', data: [{ product }] })
}



exports.updateProduct = async (req, res, next) => {
    
    const { username } = req.body
    const {id}= req.query
   // const{isAdmin}=req.body
    let product = await Product.findByIdAndUpdate(id,{$set:{username}},{new:true})
    return res.status(200).json({ message: 'Update success', UpdatedUser: [{ product }] })
}