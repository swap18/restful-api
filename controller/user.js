const User = require('../model/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')



exports.get_all_users = async (req, res, next) => {
    let user = await User.find()
    return res.status(200).json({ message: 'get_all_users success', data: [{ user }] })
}

exports.userSignup = async (req, res, next) => {
    const { email,username,isAdmin } = req.body
    

    let isUser = await User.findOne({ email })
    if (isUser) {
        return res.status(200).json({ message: 'Email already exists' })
    }
    else {
        bcrypt.hash(req.body.password, 10, async (err, hash) => {
            if (err) {
                return res.status(500).json({
                    error: err
                })
            }
            else {
                let user = await User.create({ username,email, password: hash,isAdmin })
                return res.status(200).json({ message: 'Registration Successful' })

            }
        })

    }

}

exports.deleteUser = async (req, res, next) => {


    const { id } = req.query
    let user = await User.findByIdAndRemove(id)
    return res.status(200).json({ message: 'Delete success', data: [{ user }] })
}

exports.user_Login = async (req, res, next) => {
    const { email } = req.body
    let user = await User.findOne({ email: email })
    const match = await bcrypt.compare(req.body.password, user.password)
    if (match) {
        const token = jwt.sign({
            email: user.email,
            id: user._id 
        },
            'secret',
            {
                expiresIn: '1hr'
            })

        return res.status(200).json({ message: 'Login Successful',token:token });
        

    } else {
        return res.status(200).json({ message: 'Invalid Login Credentials' });
    }

}

exports.updateUser = async (req, res, next) => {
    
    const { username } = req.body
    const {id}= req.params
   // const{isAdmin}=req.body
    let user = await User.findByIdAndUpdate(id,{$set:{username}},{new:true})
    return res.status(200).json({ message: 'Update success', UpdatedUser: [{ user }] })
}