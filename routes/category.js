var express = require('express');
var router = express.Router();

const { addCategory, getCategory,updateCategory,deleteCategory } = require('../controller/category')

router.get('/', getCategory)
router.post('/', addCategory)
router.patch('/', updateCategory)
router.delete('/', deleteCategory)


module.exports = router;